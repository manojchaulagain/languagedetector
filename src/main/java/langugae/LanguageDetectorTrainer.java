package langugae;

import java.io.File;

import opennlp.tools.langdetect.LanguageDetectorFactory;
import opennlp.tools.langdetect.LanguageDetectorME;
import opennlp.tools.langdetect.LanguageDetectorModel;
import opennlp.tools.langdetect.LanguageDetectorSampleStream;
import opennlp.tools.langdetect.LanguageSample;
import opennlp.tools.ml.perceptron.PerceptronTrainer;
import opennlp.tools.util.InputStreamFactory;
import opennlp.tools.util.MarkableFileInputStreamFactory;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.TrainingParameters;
import opennlp.tools.util.model.ModelUtil;

public class LanguageDetectorTrainer {

    public static void main(String[] args) throws Exception {
        InputStreamFactory inputStreamFactory = new MarkableFileInputStreamFactory(new File("D:\\Development\\JavaProjects\\uniparse\\src\\main\\resources\\data\\corpus.txt"));

        ObjectStream<String> lineStream =
                new PlainTextByLineStream(inputStreamFactory, "UTF-8");
        ObjectStream<LanguageSample> sampleStream = new LanguageDetectorSampleStream(lineStream);
        TrainingParameters params = ModelUtil.createDefaultTrainingParameters();
        params.put(TrainingParameters.ALGORITHM_PARAM,
                PerceptronTrainer.PERCEPTRON_VALUE);
        params.put(TrainingParameters.CUTOFF_PARAM, 0);

        LanguageDetectorFactory factory = new LanguageDetectorFactory();

        LanguageDetectorModel model = LanguageDetectorME.train(sampleStream, params, factory);
        model.serialize(new File("langdetect.bin"));
    }
}
